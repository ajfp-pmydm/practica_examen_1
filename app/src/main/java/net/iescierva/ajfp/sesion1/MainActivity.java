package net.iescierva.ajfp.sesion1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    EditText cajaNumero;
    Button boton1;
    boolean second = false;
    long num1 , num2 = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boton1 = findViewById(R.id.boton1);
        cajaNumero = findViewById(R.id.cajaNumero);
    }

    /**Los métodos públicos los asociamos a eventos como el click, los privados los usarán los públicos **/
    public void sumarMostrarDatos(View view){
        String contenido = cajaNumero.getText().toString();

        /**
        El patrón se puede definir así para poder usarse con matches distintos
        Pattern pat = Pattern.compile("\\d*");
        Matcher match = pat.matcher(contenido);
        boolean matches = match.matches();
        **/
        if (contenido.isEmpty()){
            Toast.makeText(this, "Introduzca datos, por favor.", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!second){
            num1 = Long.parseLong(contenido);
            second = true;
            boton1.setText("SUMAR");
        } else {
            num2 = Long.parseLong(contenido);
            boton1.setText("GUARDAR");
            second = false;
            /**Usamos los Intent para manejar las actividades, en este caso creamos uno para comunicar dos actividades **/
            Intent intent = new Intent(this, ResultActivity.class)
                    .putExtra("num1", num1)
                    .putExtra("num2", num2);
            this.startActivity(intent);
        }
        cajaNumero.setText("");
    }
}