package net.iescierva.ajfp.sesion1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    TextView resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Intent intent = getIntent();
        long num1 = intent.getLongExtra("num1", 0);
        long num2 = intent.getLongExtra("num2", 0);

        resultado = findViewById(R.id.resultado);
        resultado.setText("Resultado: " + Long.toString(num1 + num2));
        finishOnTime();
    }

    public void volver(View view){
        finish();
    }

    /** Se vuelve a la actividad inicial después de n segundos **/
    private void finishOnTime(){
        new CountDownTimer(6000, 1000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
                /**Al haber iniciado la actividad en otra parte con startActivity() podemos volver a la
                 * anterior simplemente finalizando con finish()**/
                finish();
            }
        }.start();
    }
}